<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: 百度网盘管理 [ 百度网盘管理 ] 
// +----------------------------------------------------------------------
// | Copyright (c) 2023 [xunmzy] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: https://www.seekxm.com
// +----------------------------------------------------------------------
// | Author: 寻梦资源网 <seekxm@qq.com>
// +----------------------------------------------------------------------
// | Date: 2024/6/29-20:17
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------
namespace seek\seekplugininstall;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;

class Installer extends LibraryInstaller
{
    
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        return parent::install($repo, $package)->then(function () use ($package) {
            $this->handlePluginConfig($package, 'install');
        });
    }
    
    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        return parent::update($repo, $initial, $target)->then(function () use ($target) {
            $this->handlePluginConfig($target, 'update');
        });
    }
    
    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        return parent::uninstall($repo, $package)->then(function () use ($package) {
            $this->handlePluginConfig($package, 'uninstall'); // 注意这里使用$package代替了$target
        });
    }
    
    protected function handlePluginConfig(PackageInterface $package, string $action)
    {
        if ($this->getIsPlugin($package)) {
            
            $configFile = 'config/plugin.php';
            if (!file_exists($configFile)) {
                touch($configFile);
                $config['debug'] = true;
                $config['auto_load'] = true;
                $config['path'] = 'plugin';
                $config['list'] = [];
    
                $config = [
                    'debug' => true,
                    'auto_load' => true,
                    'path' => 'plugin',
                    'list' => []
                ];
            } else {
                $config = include $configFile;
            }
            
            // Extract the directory name of the plugin (e.g., 'b' in 'path/to/a/b')
            $sourceDir = $this->getInstallPath($package);
            $pluginName = basename($sourceDir);
            
            if ($action === 'install' || $action === 'update') {
                $this->copyPluginFiles($package,$action);
                if (!in_array($pluginName, $config['list'])) {
                    $config['list'][] = $pluginName;
                }else{
                    $this->io->write('<info>There are duplicate plugin names: ' . $pluginName . ' please check path '.$sourceDir.'</info>');
                }
            }
            
            if ($action === 'uninstall') {
                $this->io->write('<info>scl</info>');
                $config['list'] = array_diff($config['list'], [$pluginName]);
                $config['list'] = array_values($config['list']);
                $pluginDir = isset($extra['plugin']['path']) && $extra['plugin']['path'] !== ''
                    ? rtrim($extra['plugin']['path'], '/')
                    : $this->getDefaultPluginPath();
                $targetPath = $pluginDir . '/' . $pluginName;
                $this->filesystem->remove($targetPath);
    
            }
            file_put_contents($configFile, '<?php return ' . var_export($config, true) . ';');
            
        }
    }
    
    protected function copyPluginFiles(PackageInterface $package,$action)
    {
        $this->io->write('<info>aciton info: ' . $action . '</info>');
        $extra = $package->getExtra();
        $this->io->write('<info>extra info: ' . json_encode($extra) . '</info>');
        
        // Get plugin package path
        $sourceDir = $this->getInstallPath($package);
        $this->io->write('<info>package path: ' . $sourceDir . '</info>');
        
        // Extract the directory name of the plugin (e.g., 'b' in 'path/to/a/b')
        $pluginName = basename($sourceDir);
        
        // Get project root path and determine plugin directory
        $pluginDir = isset($extra['plugin']['path']) && $extra['plugin']['path'] !== ''
            ? rtrim($extra['plugin']['path'], '/')
            : $this->getDefaultPluginPath();
        
        if (!is_dir($pluginDir)) {
            mkdir($pluginDir, 0755, true);
        }
        
        $targetPath = $pluginDir . '/' . $pluginName;
        
        $this->filesystem->copy($sourceDir, $targetPath, null, ['override' => true, 'mode' => 0755]);
    }
    
    protected function getDefaultPluginPath()
    {
        // Get the project root directory
        return 'plugin';
    }
    
    protected function getComposer()
    {
        return $this->composer;
    }
    
    protected function getIsPlugin(PackageInterface $package)
    {
        $extra = $package->getExtra();
        return isset($extra['plugin']['enabled']) && $extra['plugin']['enabled'] === true;
    }
    
}