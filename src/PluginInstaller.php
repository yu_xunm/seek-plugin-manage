<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: 百度网盘管理 [ 百度网盘管理 ] 
// +----------------------------------------------------------------------
// | Copyright (c) 2023 [xunmzy] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: https://www.seekxm.com
// +----------------------------------------------------------------------
// | Author: 寻梦资源网 <seekxm@qq.com>
// +----------------------------------------------------------------------
// | Date: 2024/7/1-03:21
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------
namespace seek\seekplugininstall;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;


/**
 * 组件插件注册
 * @class PluginInstaller
 * @package seek\seekplugininstall
 */
class PluginInstaller implements PluginInterface
{

    public function activate(Composer $composer, IOInterface $io)
    {
        $composer->getInstallationManager()->addInstaller(new Installer($io, $composer));

    }

    public function deactivate(Composer $composer, IOInterface $io)
    {
        // Clean up or perform any necessary deactivation steps here
    }
    
    public function uninstall(Composer $composer, IOInterface $io)
    {}
}
